/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Packages;

/**
 *
 * @author Pyry
 */
public class Item {
    private boolean broken=false;
    private boolean breakable;
    private double weight;
    private int size;
    private String name;
    
    public Item(String n, double w, int s, boolean b){
        name = n;
        weight = w;
        size = s;
        breakable = b;
    }
    
    public boolean isBreakable(){
        return breakable;
    }
    
    public void breakItem(){
        broken=true;
    }
    
    public boolean isBroken(){ //returns boolean of is item broken
        return broken;
    }
    
    public double getWeight(){
        return weight;
    }
    
    public int getSize(){
        return size;
    }
    
    public String toString(){
        return name;
    }
}
