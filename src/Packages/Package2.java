/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Packages;

import harjoitustyo.timo.SmartPost;

/**
 *
 * @author Pyry
 */
public class Package2 extends Parcel {  
    private final double maxSize= 300;
    private final double maxWeight=5;
    
    
    public Package2(Item i,SmartPost s, SmartPost e){
        super(i, s, e, 2);
    }
    
    @Override
    public boolean checkItem(){
        if (super.getItem().getSize()>maxSize || super.getItem().getWeight()>maxWeight)
            return false;
        return true;
    }
    
    public void setItem(Item item){
        super.setItem(item);
    }
    
}
