/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitustyo.users;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pyry
 */
public class Users {
    static private Users u = null;
    private HashMap<String, String> userMap;
    
    private Users(){ // reads existing users from file and creates hashmap of users
        userMap = new HashMap();
        try {
            File f = new File("users.txt");
            Scanner s = new Scanner(f);
            while (s.hasNextLine()){
                String[] info = s.nextLine().split(":");
                userMap.put(info[0], info[1]);
            }
            s.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Users.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static Users getInstance(){
        if (u == null )
            u = new Users();
        return u;
    }
    
    public String createUser(String userName, String password){ //Creates user to hashmap and saves new user to file
        FileWriter writer = null;
        try {
            if (userMap.containsKey(userName)) {
                return "Käyttäjänimi varattu";
            }
            if (password.length()<3)
                return "Salasana liian lyhyt (vähintään 3 merkkiä)";
            File f = new File("users.txt");
            writer = new FileWriter(f, true);
            System.out.println("avattu");
            userMap.put(userName, password);
            writer.append(userName+":"+password + "\n");
            writer.close();
            return "Käyttäjä luotu";
        } catch (IOException ex) {
            Logger.getLogger(Users.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Tapahtui virhe";
    }
    
    public boolean checkUser(String userName, String password){ //checks if user is valid
        if (userMap.containsKey(userName) && userMap.get(userName).equals(password))
            return true;
        return false;
    }
}
