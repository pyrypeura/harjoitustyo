/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitustyo;

import Packages.Item;
import Packages.ItemList;
import Packages.Package1;
import Packages.Package2;
import Packages.Package3;
import Packages.Parcel;
import Packages.Storage;
import harjoitustyo.timo.SmartPost;
import harjoitustyo.timo.SmartPosts;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import history.history;
import javafx.scene.control.Label;


/**
 * FXML Controller class
 *
 * @author Pyry
 */
public class Ui2Controller implements Initializable {

    @FXML
    private WebView webView;
    @FXML
    private ComboBox<String> cities;
    ObservableList<String> cityList = null;
    private SmartPosts sp = SmartPosts.getInstance();
    @FXML
    private ComboBox<Parcel> packages;
    private ObservableList<Parcel> packageList = null;
    Storage s = Storage.getInstance();
    @FXML
    private ComboBox<Item> ItemBox;
    @FXML
    private TextField itemName;
    @FXML
    private TextField itemWeight;
    @FXML
    private TextField itemWidth;
    @FXML
    private TextField itemHeight;
    @FXML
    private CheckBox breakable;
    @FXML
    private RadioButton class1;
    @FXML
    private RadioButton class2;
    @FXML
    private RadioButton class3;
    @FXML
    private ComboBox<String> start;
    @FXML
    private ComboBox<SmartPost> startAutomat;
    @FXML
    private ComboBox<String> target;
    @FXML
    private ComboBox<SmartPost> targetAutomat;
    ObservableList<Item> itemList = null;
    ItemList il = ItemList.getInstance();
    final ToggleGroup group = new ToggleGroup();
    @FXML
    private ListView<String> historyList;
    private history h = history.getInstance();
    @FXML
    private AnchorPane Log;
    @FXML
    private AnchorPane TIMO;
    @FXML
    private WebView testView;
    @FXML
    private AnchorPane packageControl;
    @FXML
    private Label errorLabel;
    @FXML
    private Label brokenField;
    @FXML
    private TextField itemDepth;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) { //sets up webView to index.html, sets all comboboxes to correct data and creates a togglegroup to create package window
        webView.getEngine().load(getClass().getResource("index.html").toExternalForm());
        testView.getEngine().load(getClass().getResource("index.html").toExternalForm());
        cityList = FXCollections.observableArrayList(sp.getCities());
        cities.setItems(cityList);
        if (!s.getList().isEmpty()){
            packageList=FXCollections.observableArrayList(s.getList());
            packages.setItems(FXCollections.observableArrayList(s.getList()));
        }
        start.setItems(cityList);
        target.setItems(cityList);
        itemList = FXCollections.observableArrayList(il.getList());
        ItemBox.setItems(itemList);
        class1.setToggleGroup(group);
        class2.setToggleGroup(group);
        class3.setToggleGroup(group);
        historyList.setItems(FXCollections.observableArrayList(h.readHistory()));
    }    

    @FXML
    private void addToMap(ActionEvent event) { //if user have selected a city from combobox shows citys smartpost offices in map
        ArrayList<SmartPost> posts = sp.getCitysSmartPosts(cities.getValue());
        String address;
        String info;
        for (SmartPost i : posts) {
            webView.getEngine().executeScript("document.goToLocation("+"'"+ i.getAddress() + ", " + i.getPostCode() +" "+ i.getCity()+"', '"+i.getInformation()+"', 'blue'"+")");
        }
    }

    @FXML
    private void sendPackage(ActionEvent event) { //send selected package and draws the route to map
        Parcel p = packages.getValue();
        ArrayList<String> routeArray = p.getRoute();
        int num = p.getClassNumber();
        webView.getEngine().executeScript("document.createPath(" + routeArray + ", "+ num + ", 'black'"+" )");
        p.send();
        if (p.getItem().isBroken()){
            brokenField.setText("Esine rikkoutui");
        }else {
            brokenField.setText("");
        }
        h.addHistory(p.getHistoryData());
        historyList.setItems(FXCollections.observableArrayList(h.readHistory()));
        
    }

    @FXML
    private void clearMap(ActionEvent event) { //clears routes from map
        webView.getEngine().executeScript("document.deletePaths()");
    }

    @FXML
    private void showInfo(ActionEvent event) { //opens package class info window
        try {
            Stage create = new Stage();
            Parent page = FXMLLoader.load(getClass().getResource("packageClassInfo.fxml"));
            Scene scene = new Scene(page);
            create.setScene(scene);
            create.show();
        } catch (IOException ex) {
            Logger.getLogger(loginWindowController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void setStartAutomat(ActionEvent event) { 
        startAutomat.setItems(FXCollections.observableArrayList(sp.getCitysSmartPosts(start.getValue())));
    }

    @FXML
    private void setTargetAutomat(ActionEvent event) {
        targetAutomat.setItems(FXCollections.observableArrayList(sp.getCitysSmartPosts(target.getValue())));
    }

    @FXML
    private void create(ActionEvent event) { //creates a parcel from data user has given. If user has selected item from combobox it uses that item else it creates new item from user input data. If there is error program opens error window.
        try {
            int choicedClass = 0;
            Item i = null;
            if (!itemName.getText().isEmpty() || !itemWidth.getText().isEmpty() || !itemHeight.getText().isEmpty() || !itemDepth.getText().isEmpty() ) {
                i = new Item(itemName.getText(), Integer.parseInt(itemWeight.getText()), Integer.parseInt(itemWidth.getText()) * Integer.parseInt(itemHeight.getText()) * Integer.parseInt(itemDepth.getText()), breakable.isSelected());
                il.addItem(i);
            } else if (!(ItemBox.getValue() == null)) {
                i = ItemBox.getValue();
            } else {
                errorLabel.setText("Valitse esine");
            }
            Parcel paketti = null;
            if (class1.isSelected()) {
                paketti = new Package1(i, startAutomat.getValue(), targetAutomat.getValue());
            } else if (class2.isSelected()) {
                paketti = new Package2(i, startAutomat.getValue(), targetAutomat.getValue());
            } else if (class3.isSelected()) {
                paketti = new Package3(i, startAutomat.getValue(), targetAutomat.getValue());

            }
            double distance = (double) testView.getEngine().executeScript("document.createPath(" + paketti.getRoute() + ", " + paketti.getClassNumber() + ", 'red' )");
            System.out.println(distance);
            if (paketti.getClassNumber() == 1 && distance > 150) {
                errorLabel.setText("Paketti liian iso tai painava tai matka on liian pitkä");
            } else if (!paketti.checkItem()) {
                errorLabel.setText("Paketti liian iso tai painava tai matka on liian pitkä");
            } else {
                s.addPackage(paketti);
                itemList = FXCollections.observableArrayList(il.getList());
                ItemBox.setItems(itemList);
                packageList = FXCollections.observableArrayList(s.getList());
                packages.setItems(FXCollections.observableArrayList(s.getList()));
                errorLabel.setText("");
            }
        } catch (Exception e) {
            try {
                Stage create = new Stage();
                Parent page = FXMLLoader.load(getClass().getResource("ErrorWindow.fxml"));
                Scene scene = new Scene(page);
                create.setScene(scene);
                create.show();
            } catch (IOException ex) {
                Logger.getLogger(loginWindowController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } 
    }

    @FXML
    private void eraseHistory(ActionEvent event) { //erases log history
        h.eraseHistory();
        historyList.setItems(FXCollections.observableArrayList(h.readHistory()));
    }
    
}
