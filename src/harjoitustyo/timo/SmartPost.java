/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitustyo.timo;

/**
 *
 * @author Pyry
 */
public class SmartPost {
    private String city;
    private String name;
    private String address;
    private String postNumber;
    private GeoPoint point;
    private String hours;
    
    
    public SmartPost(String c, String n, String a, String p, String la, String lo, String h){
        city = c;
        name = n;
        address = a;
        postNumber = p;
        point = new GeoPoint(la, lo);
        hours = h;
    }
    
    public String getName(){
        return name;
    }
    
    public String getCity(){
        return city;
    }
    
    public String getAddress(){
        return address ;
    }
    
    public String getInformation(){ //returns opening hours and name of smart post office
        return name + hours;
    }
    
    public String getPostCode(){
        return postNumber;
    }
    
    public GeoPoint getGeoPoint(){
        return point;
    }
    
    public String toString(){
        return name;
    }
}
